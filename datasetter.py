#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import requests
import urllib.request
from typing import Dict, Set, Tuple
import bs4
import json
import pandas as pd
import os

def get_soup(link: str) -> bs4.BeautifulSoup:
	resp = requests.get(link)
	return bs4.BeautifulSoup(resp.text, 'html.parser')


def set_info(soup: bs4.BeautifulSoup) -> Dict:
	concerts = soup.find_all(class_='setlist')

	all_dates = {}
	datum = ['setlistdate', 'venue', 'city', 'state', 'country']
	for concert in concerts:
		info = concert.find('h3').find_all('a')
		cur = {
			d: i.string
			for d, i in zip(datum, info)
		}
		for part in concert.find_all('p'):
			try:
				name = part.find('b').string.replace(':', '').replace(' ', '')
			except:
				continue
			cur[name] = [
				s['href']
				for s in part.find_all('a')
			]
		all_dates[cur['setlistdate']] = cur

	return all_dates


def song_explorer(song_ext: str) -> Dict:
	soup = get_soup('http://allthings.umphreys.com' + song_ext)

	content = soup.find('div',style=("font-size:120%;font-weight:bold;"
									"margin:10px 0;padding:0;color:#666;")).contents

	if 'was originally performed by' in content[1]:
		artist = content[1].replace('was originally performed by', '').strip()
	else:
		artist = 'Umphreys McGee'

	return {
		'artist': artist,
		'song': content[0].text,
	}


def song_set(df: pd.DataFrame) -> Set[str]:
	songs = set()
	containers = [
		'Encore', 
		'OneSet', 
		'Overtime', 
		'Quarter1', 
		'Quarter2', 
		'Quarter3', 
		'Quarter4', 
		'Set1',
		'Set2', 
		'Set3', 
	]

	for container in containers:
		songs.update([s for row in df[container].dropna() for s in row if '/song/' in s])
	
	return songs


def get_concerts(start: int=1998, end: int=2019, save: str=True) -> Dict:
	base = 'http://allthings.umphreys.com/setlists/'
	all_concerts = {}

	for year in range(start, end+1):
		link = f'{base}{year}.html'

		concerts = set_info(get_soup(link))
		all_concerts.update(concerts)
	
	if save:
		with open('./ump_data.json', 'w') as f:
			json.dump(all_concerts, f)
	
	return all_concerts


def get_songs(save: str=True) -> Dict:
	df = pd.read_json('ump_data.json', orient='index')

	unique_songs = song_set(df)

	song_info = {}
	for song in unique_songs:
		song_info[song] = song_explorer(song)

	if save:
		with open('ump_songs.json', 'w') as f:
			json.dump(song_info, f)
	
	return song_info


def get_data(concert_path: str='./ump_data.json', 
			song_path: str='./ump_songs.json') -> Tuple[Dict, Dict]:

	if os.path.exists(concert_path):
		concerts = json.load(open(concert_path, 'r'))
	else:
		print('No concert file. Getting now...')
		concerts = get_concerts()
	
	if os.path.exists(song_path):
		songs = json.load(open(song_path, 'r'))
	else:
		print('No song file. Getting now...')
		songs = get_songs()
	
	return concerts, songs


def datasetter(save: str=True) -> Dict:
	concerts, songs = get_data()

	concert_info = [
		'city', 
		'country', 
		'setlistdate', 
		'state', 
		'venue',
	]

	containers = [
		'Encore', 
		'OneSet', 
		'Overtime', 
		'Quarter1', 
		'Quarter2', 
		'Quarter3', 
		'Quarter4', 
		'Set1',
		'Set2', 
		'Set3', 
	]

	dataset = []
	for _, concert in concerts.items():
		cur = {
			k: concert[k] for k in concert_info if k in concert.keys()
		}
		for part in list(set(concert).intersection(containers)):
			part_songs = [
				{
					**cur, 
					**songs[song], 
					**{'track': i,
						'section': part}
				} for i, song in enumerate(concert[part])
			]

			dataset.extend(part_songs)
	
	if save:
		json.dump(dataset, open('ump_dataset.json', 'w'))

	return dataset

def feature_maker():
	df = pd.read_json('./dataset.json')

	artist_ids = {
		a: i
		for i, a in enumerate(df.artist.unique().tolist())
	}
	df['artist_id'] = df.artist.map(artist_ids)

	song_ids = {
		s: i
		for i, s in enumerate(df.song.unique().tolist())
	}
	df['song_id'] = df.song.map(song_ids)


if __name__=='__main__':
	datasetter()